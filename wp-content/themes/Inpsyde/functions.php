<?php
function twentyseventeen_widgets_init()
{
    register_sidebar(
        array(
            'name'          => __('Blog Sidebar', 'twentyseventeen'),
            'id'            => 'sidebar-1',
            'description'   => __('Add widgets here to appear in your sidebar on blog posts and archive pages.', 'twentyseventeen'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    register_sidebar(
        array(
            'name'          => __('Footer 1', 'twentyseventeen'),
            'id'            => 'sidebar-2',
            'description'   => __('Add widgets here to appear in your footer.', 'twentyseventeen'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    register_sidebar(
        array(
            'name'          => __('Footer 2', 'twentyseventeen'),
            'id'            => 'sidebar-3',
            'description'   => __('Add widgets here to appear in your footer.', 'twentyseventeen'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );
}
add_action('widgets_init', 'twentyseventeen_widgets_init');
?>

<?php
// add post thumbnails
add_theme_support('post-thumbnails');
?>
<?php
// menu
function register_my_menus()
{
    register_nav_menus(
        array(
            'header-menu' => __('Header Menu'),
            'extra-menu' => __('Extra Menu')
        )
    );
}
add_action('init', 'register_my_menus');
?>
<?php
add_action('wp_enqueue_scripts', 'child_enqueue_styles');

function child_enqueue_styles()
{

    wp_enqueue_style('reset-style', get_template_directory_uri() . '/css/reset.css', array());
} ?>
