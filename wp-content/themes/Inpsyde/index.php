<?php get_header(); ?>

<body>
  <section class="content">
    <?php
    if (have_posts()) {
      while (have_posts()) {
        the_post();
    ?>
        <div class="row">
          <div class="col-md-9">
            <a href="<?php the_permalink(); ?>">

              <h1 class="content-title"><?php the_title(); ?></h1>
            </a>
            <p class="content-descroption"><?php the_content(); ?></p>
            <p class="content-author"><?php the_time('l, F jS, Y'); ?> > <?php the_author(); ?></p>
            <hr>
          </div>
          <div class="col-md-3">
            <img class="image" <?php the_post_thumbnail(); ?> </div> <hr>
          </div>
      <?php
      }
    }
      ?>
  </section>

  </div>
  </div>
  </section>
</body>
<?php get_footer(); ?>